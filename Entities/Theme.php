<?php

namespace Modules\Themes\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Setting;

class Theme extends Model
{
    protected $fillable = [];

	public static function getThemeName()
	{
		$theme = null;

		if($getTheme = Setting::whereModule('core')->whereName('theme')->first()){
			$theme = $getTheme->value;
		}

		return $theme;
	}
}
