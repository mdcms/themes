<?php

namespace Modules\Themes\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Theme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
		\Theme::set(\Modules\Themes\Entities\Theme::getThemeName());

        return $next($request);
    }
}
